import React, { useState } from 'react'
import PropTypes from 'prop-types'
import './Cart.css'
import deleteImg from '../../assets/deleteicon.png'

function Cart(props) {
	const { setCartValue, cartValue, removeFromCart, updateCart } = props

	function totalPrice() {
		return cartValue.reduce((acc, curr) => {
			return (acc += curr.price * curr.quantity)
		}, 0)
	}
	return (
		<div className='cart_section'>
			<h1 style={{ marginTop: '10px' }}>cart</h1>
			<div className='card_item_container'>
				{cartValue.length == 0 && (
					<div className='cart_message' style={{ textAlign: 'center' }}>
						cart is Empty
					</div>
				)}
				{cartValue &&
					cartValue
						.sort((a, b) => a.id - b.id)
						.map((item) => {
							return (
								<SingleItem
									key={item.id}
									item={item}
									setCartValue={setCartValue}
									removeFromCart={removeFromCart}
									updateCart={updateCart}
								/>
							)
						})}
			</div>
			<div className='cart_total'>
				<span className='cart_total_value'>
					total : {'$ ' + totalPrice().toFixed(2)}
				</span>
				<div
					className='cart_order_btn clear_btn'
					onClick={() => {
						setCartValue([])
					}}
				>
					clear cart
				</div>
			</div>
			{cartValue.length > 0 && (
				<div
					className='cart_order_btn'
					onClick={() => {
						setCartValue([])
						alert('your order is is placed')
					}}
				>
					order Now
				</div>
			)}
		</div>
	)
}

Cart.propTypes = {
	setCartValue: PropTypes.func,
	cartValue: PropTypes.array,
	removeFromCart: PropTypes.func,
	updateCart: PropTypes.func,
}

function SingleItem(props) {
	const {
		updateCart,
		removeFromCart,
		item: { title, price, image, quantity },
	} = props
	const [quantityCart, setQuantityCart] = useState(quantity)
	return (
		<div className='cart_item'>
			<img
				height='80px'
				width='80px'
				src={image}
				alt='image'
				className='card_product_image'
			/>
			<div className='cart_product_title'> {title.slice(0, 40)} </div>
			<div className='counter_container'>
				<div
					className='plus_btn btn'
					onClick={() => {
						setQuantityCart((prev) => (prev < 10 ? prev + 1 : prev))
						updateCart({
							...props.item,
							quantity: quantityCart < 10 ? quantityCart + 1 : quantityCart,
						})
					}}
				>
					+
				</div>
				<span>{quantityCart}</span>
				<div
					className='minus_btn btn'
					onClick={() => {
						setQuantityCart((prev) => (prev > 1 ? prev - 1 : prev))
						updateCart({
							...props.item,
							quantity: quantityCart > 1 ? quantityCart - 1 : quantityCart,
						})
					}}
				>
					-
				</div>
			</div>
			<div className='cart_product_price'>
				{' '}
				{'$ ' + (price * quantityCart).toFixed(2)}{' '}
			</div>
			<div>
				<img
					className='card_product_removebtn'
					onClick={() => removeFromCart(props.item)}
					height='40px'
					width='40px'
					src={deleteImg}
					alt='remove'
				/>
			</div>
		</div>
	)
}

SingleItem.propTypes = {
	updateCart: PropTypes.func,
	removeFromCart: PropTypes.func,
	item: PropTypes.shape({
		id: PropTypes.number,
		title: PropTypes.string,
		price: PropTypes.number,
		quantity: PropTypes.number,
		image: PropTypes.string,
	}),
}
export default Cart
