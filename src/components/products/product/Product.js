import React, { useEffect, useState } from 'react'
import './Product.css'
import PropTypes from 'prop-types'

function Product(props) {
	const {
		id,
		title,
		description,
		price,
		image,
		rating: { rate },
	} = props.product
	const { addToCart, cartValue, removeFromCart } = props
	const [toggle, setToggle] = useState(false)
	const [quantity, setQuantity] = useState(1)
	useEffect(() => {
		const exists = cartValue.filter((cartItem) => cartItem.id === id)
		if (exists.length > 0) {
			setToggle(false)
			setQuantity(exists[0].quantity)
		} else setToggle(true)
	}, [cartValue])
	return (
		<div className='product_container'>
			<h3 className='product_title'>{title}</h3>
			<img
				height='90px'
				width='90px'
				src={image}
				alt={title}
				className='product_image'
			/>
			<div className='product_details'>
				<p className='product_description'>
					{description.slice(0, 70) + '...'}
				</p>
				<div className='mid_section'>
					<p className='product_price'>{'$ ' + price}</p>
					{toggle && (
						<div className='counter_container'>
							<div
								className='plus_btn btn'
								onClick={() =>
									setQuantity((prev) => (prev < 10 ? prev + 1 : prev))
								}
							>
								+
							</div>
							<span>{quantity}</span>
							<div
								className='minus_btn btn'
								onClick={() =>
									setQuantity((prev) => (prev > 1 ? prev - 1 : prev))
								}
							>
								-
							</div>
						</div>
					)}
				</div>

				<div className='product_bottom_sec'>
					{toggle ? (
						<div
							className='addCart_btn'
							onClick={() => {
								addToCart({
									...props.product,
									quantity,
								})
								setToggle((val) => !val)
							}}
						>
							add to cart
						</div>
					) : (
						<div
							className='removeCart_btn'
							onClick={() => {
								removeFromCart(props.product)
								setToggle((val) => !val)
							}}
						>
							remove from cart
						</div>
					)}

					<p className='product_rating'>{rate + ' / 5'}</p>
				</div>
			</div>
		</div>
	)
}

Product.propTypes = {
	product: PropTypes.shape({
		id: PropTypes.number,
		title: PropTypes.string,
		price: PropTypes.number,
		description: PropTypes.string,
		image: PropTypes.string,
		category: PropTypes.string,
		rating: PropTypes.shape({
			rate: PropTypes.number,
		}),
	}),
	addToCart: PropTypes.func,
	removeFromCart: PropTypes.func,
	cartValue: PropTypes.array,
}

export default Product
