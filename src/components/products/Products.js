import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import './Products.css'
import Product from './product/Product'

function Products(props) {
	const { products, search, setSearch, loading } = props

	/* NOTE another method to search products every 5 sec after writing  */

	const [value, setValue] = useState('')
	const searchItem = (e) => {
		setValue(e.target.value.toLowerCase())
	}

	useEffect(() => {
		if (value && products?.length > 0) {
			const timeoutId = setTimeout(() => {
				if (!products) return
				const result =
					products &&
					products.length > 0 &&
					products.filter((product) => {
						return product.title.toLowerCase().includes(value)
					})
				setSearch(result)
			}, 3000)
			return () => clearTimeout(timeoutId)
		} else {
			setSearch(products)
		}
	}, [value])

	/* NOTE another method to search products instant after writing  */

	// function searchItem(e) {
	// 	const result = products.filter((product) => {
	// 		return product.title.toLowerCase().includes(e.target.value.toLowerCase())
	// 	})
	// 	setSearch(result)
	// }
	return (
		<div>
			<div className='product_section'>
				<div className='product_search'>
					<input
						type='text'
						className='search'
						onChange={searchItem}
						placeholder='Search........'
					/>
				</div>
				<div className='products_container_main'>
					{loading && <div style={{ marginTop: '20px' }}>Loading....</div>}
					<div className='products_container'>
						{search?.length == 0 && (
							<div style={{ marginTop: '20px' }}>No results found</div>
						)}
						{search &&
							search.map((product) => {
								return <Product key={product.id} product={product} {...props} />
							})}
					</div>
				</div>
			</div>
		</div>
	)
}

Products.propTypes = {
	products: PropTypes.array,
	search: PropTypes.array,
	setSearch: PropTypes.func,
	loading: PropTypes.bool,
}
export default Products
