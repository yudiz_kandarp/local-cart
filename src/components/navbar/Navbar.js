import React from 'react'
import PropTypes from 'prop-types'
import './Navbar.css'

function Navbar(props) {
	const { setTab, tab, cartValue } = props
	return (
		<div className='navbar'>
			<div className='navbar_title'>Local Cart</div>
			<div className='navbar_links'>
				{tab === 'cart' ? (
					<div
						className='navbar_btn'
						onClick={() => {
							setTab('home')
						}}
					>
						home
					</div>
				) : (
					<div
						className='navbar_btn'
						onClick={() => {
							setTab('cart')
						}}
					>
						{'cart ' + (cartValue.length ? '(' + cartValue.length + ')' : '')}
					</div>
				)}
			</div>
		</div>
	)
}
Navbar.propTypes = {
	tab: PropTypes.string,
	setTab: PropTypes.func,
	cartValue: PropTypes.array,
}
export default Navbar
