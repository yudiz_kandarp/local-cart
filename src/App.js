import React, { useEffect, useState } from 'react'
import Products from './components/products/Products'
import Navbar from './components/navbar/Navbar'
import Cart from './components/cart/Cart'

function App() {
	const [tab, setTab] = useState('home')
	const [products, setProducts] = useState()
	const [search, setSearch] = useState([])
	const [loading, setLoading] = useState(true)
	const [cartValue, setCartValue] = useState([])

	useEffect(() => {
		fetchProducts()
	}, [])
	async function fetchProducts() {
		try {
			const res = await fetch('https://fakestoreapi.com/products')
			const products = await res.json()
			setProducts(products)
			setSearch(products)
			setLoading(false)
		} catch (error) {
			console.log('network error :', error)
		}
	}

	function addToCart(item) {
		const exists = cartValue.find((value) => item.id === value.id)
		if (exists) return
		setCartValue((prev) => [...prev, item])
	}
	function updateCart(item) {
		const exists = cartValue.find((value) => item.id === value.id)
		if (exists) {
			const removedItem = cartValue.filter((value) => item.id !== value.id)
			exists.quantity = item.quantity
			setCartValue([...removedItem, exists])
		}
	}

	function removeFromCart(item) {
		const exists = cartValue.find((value) => item.id === value.id)
		if (exists) {
			const removedItem = cartValue.filter((value) => item.id !== value.id)
			setCartValue(removedItem)
		}
	}

	return (
		<div>
			<Navbar tab={tab} setTab={setTab} cartValue={cartValue} />
			{tab === 'home' ? (
				<Products
					setCartValue={setCartValue}
					cartValue={cartValue}
					addToCart={addToCart}
					removeFromCart={removeFromCart}
					products={products}
					setSearch={setSearch}
					search={search}
					loading={loading}
				/>
			) : (
				<Cart
					setCartValue={setCartValue}
					cartValue={cartValue}
					removeFromCart={removeFromCart}
					updateCart={updateCart}
				/>
			)}
		</div>
	)
}

export default App
